from microbit import *

CYCLE_MINUTES = 20
LED_INTENSITY_OFF = 0
LED_INTENSITY_ON_BLINKING = 3
LED_INTENSITY_ON_CONSTANT = 1
LED_INTENSITY_ON = 3
ROWS = 5
SECONDS_IN_LED = 60

while True:
    timer_stopped_status = False
    big_x = 0
    big_y = 4
    if button_a.is_pressed():
        led_x = 0
        led_y = 0
        for minutes in range(CYCLE_MINUTES):
            for seconds in range(SECONDS_IN_LED):
                if button_b.was_pressed():
                    timer_stopped_status = True
                    break
                display.set_pixel(led_x, led_y, LED_INTENSITY_ON)
                sleep(500)
                display.set_pixel(led_x, led_y, LED_INTENSITY_OFF)
                sleep(500)
            if timer_stopped_status:
                display.clear()
                timer_stopped_status = False
                break
            display.set_pixel(led_x, led_y, LED_INTENSITY_ON)
            led_x = (led_x + 1) % ROWS
            if led_x == 0:
                if led_y == 3:
                    display.clear()
                    display.set_pixel(big_x, big_y, LED_INTENSITY_ON)
                    big_x = (big_x + 1) % ROWS
                    led_x = 0
                    led_y = 0
                else:
                    led_y = led_y + 1
